var playlist = [];
var currentSong = 0;

window.onload = function(){
    SC.initialize({
        client_id: '344397d4cc2a616ed20f8da985e322d0' 
    });
    player = document.getElementById('soundcloud_widget');
    widget = SC.Widget(player);
    //widget.bind(SC.Widget.Events.FINISH, playNext());
    widget.bind(SC.Widget.Events.FINISH, function(){
        currentSong++;
        if(playlist[currentSong]){
            loadSong(playlist[currentSong]);
        }
    });
};

function loadSong(url){
    widget.load(url, {auto_play: true});
}

function playAll(arr){
    currentSong = 0;
    playlist = arr.split(",");
    loadSong(playlist[currentSong]);
}