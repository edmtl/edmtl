exports.schema = {
    username: String,
    password: String,
    name: {
        first: String,
        last: String
    },
    signupDate: { type: Date, default: Date.now },
    admin: {type: Boolean, default: false}
};

exports.virtuals = {
    'name.full': {
        get: function() {
            return this.name.first+ ' ' + this.name.last;
        }
    }
};