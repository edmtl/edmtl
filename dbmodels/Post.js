exports.schema = {
	title: String,
    name: String,
    artist: Array,
    remix: Array,
    genre: Array,
    url: String,
    description: String,
    blogger: String,
    date: { type: Date, default: Date.now }
};
