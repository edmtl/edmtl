var express = require('express');
var router = express.Router();
var db = require('mongoose-simpledb').db;
/* GET home page. */
router.get('/', function(req, res) {
    res.render('addPost', {title: 'Add A Post'});
});

router.post('/createNew', function(req,res){
    var song = new db.Posts({
        description: req.body.description,
        blogger: req.body.blogger
        });
    song.save(function (err,song){
        if(err) return console.error(err);
        res.send(song);
    });
});

module.exports = router;
