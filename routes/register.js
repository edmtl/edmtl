var express = require('express');
var router = express.Router();
var db = require('mongoose-simpledb').db;
/* GET home page. */
router.get('/', function(req, res) {
    res.render('register', {title: 'Register'});
});

router.post('/createUser', function(req,res){
	var user = new db.User({
		username: req.body.username,
		password: req.body.password,
		name: {
			first: req.body.firstName,
			last: req.body.lastName
		}
	});
	user.save(function (err,user){
		if(err) return console.error(err);
		res.send(user);
	});
});

module.exports = router;
