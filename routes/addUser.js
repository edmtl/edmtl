var express = require('express');
var router = express.Router();
var db = require('mongoose-simpledb').db;
/* GET home page. */
router.get('/', function(req, res) {
    res.render('addUser', {title: 'Register Now'});
});

router.post('/createNew', function(req,res){
    var user = new db.User({
        username: req.body.username,
        password: req.body.password,
        name: {
            first: req.body.firstname,
            last: req.body.lastname
            }   
        });
    user.save(function (err,user){
        if(err) return console.error(err);
        res.send(user);
    });
});

module.exports = router;
