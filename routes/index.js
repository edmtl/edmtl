var express = require('express');
var router = express.Router();
var db = require('mongoose-simpledb').db;
/* GET home page. */
router.get('/', function(req, res) {

    db.Post.find({}, function(err, posts) {
        var songs = [];
        posts = posts.reverse();
        posts.forEach(function(post){
            songs.push(post.url);
        });
        if (err) return console.error(err);
        res.render('index', {title: 'EDMTL',
                        postArray: posts,
                        songArray: songs});
    });

});

module.exports = router;
