var express = require('express');
var router = express.Router();
var db = require('mongoose-simpledb').db;
/* GET home page. */
router.get('/', function(req, res) {
    res.render('addSong', {title: 'Add A Song'});
});

router.post('/createNew', function(req,res){
    var song = new db.Post({
        name: req.body.songname,
        artist: req.body.artist.split(","),
        remix: req.body.remix,
        genre: req.body.genre,
        url: req.body.url,
        description: req.body.description,
        blogger: req.body.blogger
        });
    song.save(function (err,song){
        if(err) return console.error(err);
        res.send(song);
    });
});

router.get('/:songid', function(req,res){
    db.Post.find({_id: req.param('songid')}, function(err, song){
        if (err) return console.error(err);
        res.render('song', {title: song[0].name+" - "+song[0].artist,
                            songInfo: song[0]});
    });
});

module.exports = router;
